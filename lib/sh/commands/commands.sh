#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file commands.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-01-25 Tuesday 21:45:53 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-05-19 Tuesday 13:55:50 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Compatibility Checks {{{
##  ==========================================================================
(
  __fail__() {
    printf -- '%s: %s: %s\n' 'error' "${0##*/}" 'Incompatible shell' 1>&2
    exit 1
  }
  ##
  __trap__() {
    trap -- - EXIT
  }
  ##
  trap -- __fail__ EXIT
  __trap__
) || return 1
##  ==========================================================================
##  }}} Compatibility Checks
##
### Internal Functions {{{
##  ==========================================================================
__pcomm__() {
  ps -o comm= "${1-${$}}"
}
##
__basename__() {
  command echo "${1##*/}"
}
##
__reallink__() (
  link="${1}"
  path="${1}"
  [ -n "${path}" ] || return "${?}"
  while [ -L "${path}" ]; do
    dir="${path%/*}"
    if [ "${dir}" = "${path}" ]; then
      dir='.'
    fi
    CDPATH= command cd -- "${dir}/" || return "${?}"
    dir="$(command pwd -P)" || return "${?}"
    CDPATH= command cd -- "${dir}/" || return "${?}"
    link="${path##*/}"
    path="$(command readlink "${link}")" || return "${?}"
  done
  dir="${link%/*}"
  if [ "${dir}" = "${link}" ]; then
    dir='.'
  fi
  CDPATH= command cd -- "${dir}/" || return "${?}"
  dir="$(command pwd -P)" || return "${?}"
  dir="${dir%/}"
  link="${link##*/}"
  if   [ "${link}" = '.'  ]; then
    link=
  elif [ "${link}" = '..' ]; then
    dir="${dir%/*}"
    link=
  fi
  command echo "${dir:-/}${link:+${dir:+/}}${link}"
)
##
__realpath__() (
  path="${1}"
  [ -n "${path}" ] || return "${?}"
  while [ -L "${path}" ]; do
    dir="${path%/*}"
    if [ "${dir}" = "${path}" ]; then
      dir='.'
    fi
    CDPATH= command cd -- "${dir}/" || return "${?}"
    dir="$(command pwd -P)" || return "${?}"
    CDPATH= command cd -- "${dir}/" || return "${?}"
    path="${path##*/}"
    path="$(command readlink "${path}")" || return "${?}"
  done
  dir="${path%/*}"
  if [ "${dir}" = "${path}" ]; then
    dir='.'
  fi
  CDPATH= command cd -- "${dir}/" || return "${?}"
  dir="$(command pwd -P)" || return "${?}"
  dir="${dir%/}"
  path="${path##*/}"
  if   [ "${path}" = '.'  ]; then
    path=
  elif [ "${path}" = '..' ]; then
    dir="${dir%/*}"
    path=
  fi
  command echo "${dir:-/}${path:+${dir:+/}}${path}"
)
##
__abspath__() (
  path="${1}"
  [ -n "${path}" ] || return "${?}"
  dir="${path%/*}"
  if [ "${dir}" = "${path}" ]; then
    dir='.'
  fi
  CDPATH= command cd -- "${dir}/" || return "${?}"
  dir="$(command pwd)" || return "${?}"
  dir="${dir%/}"
  path="${path##*/}"
  if   [ "${path}" = '.'  ]; then
    path=
  elif [ "${path}" = '..' ]; then
    dir="${dir%/*}"
    path=
  fi
  command echo "${dir:-/}${path:+${dir:+/}}${path}"
)
##  ==========================================================================
##  }}} Internal Functions
##
### Functions {{{
##  ==========================================================================
command_name() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  noerr command -v "${1}"
}
##
# ----------------------------------------------------------------------------
# CAUTION:
#
# The following statement from the POSIX.1 standard (IEEE Std 1003.1, The Open
# Group Base Specifications) implies that (any) signal(s) may be ignored prior
# entry to a shell script:
#
# > Signals that were ignored on entry to a non-interactive shell cannot be
# > trapped or reset, although no error need be reported when attempting to do
# > so.  [1]
#
# A good example of it would be <systemd.exec(5)> option 'IgnoreSIGPIPE':
#
# >    IgnoreSIGPIPE=
# >        Takes a boolean argument. If true, causes SIGPIPE to be ignored in
# >        the executed process. Defaults to true because SIGPIPE generally is
# >        useful only in shell pipelines.  [2]
#
# That is, by default, shell script(s) run by <systemd.service(5)> will output
#
#   trap -- '' PIPE
#
# upon calling the 'trap' command and the 'PIPE' signal cannot be either
# trapped or reset, though no error need be reported when attempting to do so.
#
# Hence, setting
#
#   IgnoreSIGPIPE=no
#
# if shell pipeline(s) are expected to be run by <systemd.service(5)>,
# especially if they need to handle (trap) the 'PIPE' signal, is a good idea
# as implicitly suggested in [2].
#
trapped() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  trap -- - KILL || :
  set -- "$(trap)"
  [ -n "${1}" ] && [ "${1}" != "trap -- '' PIPE" ]
}
##
traps() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  __force__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -f|--force)
        __force__=1
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  assert [ "${#}" -gt 0 ] || return "${?}"
  __action__="${1}"
  shift
  [ "${#}" -gt 0 ] || set -- EXIT ABRT ALRM HUP INT QUIT TERM # PIPE
  if [ -z "${__action__}" ] || [ "${__action__}" = '-' ]; then
    trap -- "${__action__}" "${@}" && set -- "${?}" || set -- "${?}"
    unset -v __action__
    unset -v  __force__
    return "${1}"
  fi
  if [ -z "${__force__}" ]; then
    ! trapped || return "${?}"
  fi
  for __sig__ in "${@}"; do
    __sig__="${__sig__#SIG}"
    case "${__sig__}" in
      DEBUG|ERR|EXIT|RETURN)
        trap -- "${__action__}" "${__sig__}"
        ;;
      *)
        trap -- '
toerr echo "Received '"SIG${__sig__}"' (${?})"
trap -- - EXIT || :
trap -- - '"${__sig__}"' || :
'"${__action__}"'
trap -- - EXIT || :
trap -- - '"${__sig__}"' || :
getpid __pid__ && kill -s '"${__sig__}"' -- "${__pid__}"' "${__sig__}"
        ;;
    esac || {
      set -- "${?}"
      unset -v __action__
      unset -v  __force__
      unset -v    __sig__
      trap -- - "${@}" || :
      return "${1}"
    }
  done
  unset -v __action__
  unset -v  __force__
  unset -v    __sig__
}
##
trap_or_exit() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  __force__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -f|--force)
        __force__=1
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  assert [ "${#}" -gt 0 ] || return "${?}"
  __action__="${1}"
  shift
  [ "${#}" -gt 0 ] || set -- EXIT ABRT ALRM HUP INT QUIT TERM # PIPE
  __exit__=
  for __sig__ in "${@}"; do
    __sig__="${__sig__#SIG}"
    case "${__sig__}" in
      EXIT)
        if [ -n "${__action__}" ] && [ "${__action__}" != '-' ]; then
          __exit__='
toerr echo "Exited (${?})"
'"${__action__}"
        fi
        break
        ;;
    esac
  done
  unset -v __sig__
  traps ${__force__:+-f} -- "${__action__}" "${@}" &&
    if [ -n "${__exit__}" ]; then
      traps -f -- "${__exit__}" EXIT
    fi || {
      set -- "${?}"
      toerr echo "Exited (${1})"
      if [ -n "${__exit__}" ]; then
        trap -- - EXIT || :
        eval -- "${__action__}"
        trap -- - EXIT || :
      fi
      exit "${1}"
    }
  unset -v __action__
  unset -v   __exit__
  unset -v  __force__
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
pgid() {
  if [ "${#}" -lt 1 ]; then
    getpid __pid__ || return "${?}"
    set -- "${__pid__}"
    unset -v __pid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    set -- "$(ps -o pgid= "${1}")" "${?}"
    if [ "${2}" -eq 0 ]; then
      echo "${1}" | tr -d ' '
    else
      return "${2}"
    fi
  else
    return "${?}"
  fi
}
##
ppid() {
  if [ "${#}" -lt 1 ]; then
    getpid __pid__ || return "${?}"
    set -- "${__pid__}"
    unset -v __pid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    set -- "$(ps -o ppid= "${1}")" "${?}"
    if [ "${2}" -eq 0 ]; then
      echo "${1}" | tr -d ' '
    else
      return "${2}"
    fi
  else
    return "${?}"
  fi
}
##
puid() {
  if [ "${#}" -lt 1 ]; then
    getpid __pid__ || return "${?}"
    set -- "${__pid__}"
    unset -v __pid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    set -- "$(ps -o uid= "${1}")" "${?}"
    if [ "${2}" -eq 0 ]; then
      echo "${1}" | tr -d ' '
    else
      return "${2}"
    fi
  else
    return "${?}"
  fi
}
##
puser() {
  if [ "${#}" -lt 1 ]; then
    getpid __pid__ || return "${?}"
    set -- "${__pid__}"
    unset -v __pid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    set -- "$(ps -o user= "${1}")" "${?}"
    if [ "${2}" -eq 0 ]; then
      echo "${1}"
    else
      return "${2}"
    fi
  else
    return "${?}"
  fi
}
##
getpid() {
  if [ -n "${1-}" ]; then
    set -- "${1}" "$(exec -- sh -e -u -c 'echo "${PPID}"')" "${?}"
    if [ "${3}" -eq 0 ]; then
      if noerr [ "${2}" -gt 0 ]; then
        eval -- "${1}=${2}"
      else
        return "${?}"
      fi
    else
      return "${3}"
    fi
  else
    return "${?}"
  fi
}
##
getpgid() {
  if [ -n "${1-}" ]; then
    if [ "${#}" -lt 2 ]; then
      getpid __pid__ || return "${?}"
      set -- "${1}" "${__pid__}"
      unset -v __pid__
    fi
    if noerr [ "${2}" -gt 0 ]; then
      set -- "${1}" "$(pgid "${2}")" "${?}"
      if [ "${3}" -eq 0 ]; then
        eval -- "${1}=${2}"
      else
        return "${3}"
      fi
    else
      return "${?}"
    fi
  else
    return "${?}"
  fi
}
##
getppid() {
  if [ -n "${1-}" ]; then
    if [ "${#}" -lt 2 ]; then
      getpid __pid__ || return "${?}"
      set -- "${1}" "${__pid__}"
      unset -v __pid__
    fi
    if noerr [ "${2}" -gt 0 ]; then
      set -- "${1}" "$(ppid "${2}")" "${?}"
      if [ "${3}" -eq 0 ]; then
        eval -- "${1}=${2}"
      else
        return "${3}"
      fi
    else
      return "${?}"
    fi
  else
    return "${?}"
  fi
}
##
getpuid() {
  if [ -n "${1-}" ]; then
    if [ "${#}" -lt 2 ]; then
      getpid __pid__ || return "${?}"
      set -- "${1}" "${__pid__}"
      unset -v __pid__
    fi
    if noerr [ "${2}" -gt 0 ]; then
      set -- "${1}" "$(puid "${2}")" "${?}"
      if [ "${3}" -eq 0 ]; then
        eval -- "${1}=${2}"
      else
        return "${3}"
      fi
    else
      return "${?}"
    fi
  else
    return "${?}"
  fi
}
##
getpuser() {
  if [ -n "${1-}" ]; then
    if [ "${#}" -lt 2 ]; then
      getpid __pid__ || return "${?}"
      set -- "${1}" "${__pid__}"
      unset -v __pid__
    fi
    if noerr [ "${2}" -gt 0 ]; then
      set -- "${1}" "$(puser "${2}")" "${?}"
      if [ "${3}" -eq 0 ]; then
        eval -- "${1}=${2}"
      else
        return "${3}"
      fi
    else
      return "${?}"
    fi
  else
    return "${?}"
  fi
}
##
killpg() {
  if [ "${#}" -lt 1 ]; then
    getpgid __pgid__ || return "${?}"
    set -- "${__pgid__}"
    unset -v __pgid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    kill -- "-${1}"
  else
    return "${?}"
  fi
}
##
killpp() {
  if [ "${#}" -lt 1 ]; then
    getppid __ppid__ || return "${?}"
    set -- "${__ppid__}"
    unset -v __ppid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    kill --  "${1}"
  else
    return "${?}"
  fi
}
##
ispplive() {
  if [ "${#}" -lt 1 ]; then
    set -- "${PPID}" "$(ppid "${$}")"
  else
    getppid __ppid__ || return "${?}"
    set -- "${1}" "${__ppid__}"
    unset -v __ppid__
  fi
  noerr [ "${1}" -gt 0 ] && noerr [ "${1}" -eq "${2}" ]
}
##
isppdead() {
  ! ispplive
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
pw() {
  getent passwd "${@}"
}
##
pwuser() {
  set -- "$(pw "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}" | cut -d ':' -f 1
  else
    return "${2}"
  fi
}
##
pwuid() {
  set -- "$(pw "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}" | cut -d ':' -f 3
  else
    return "${2}"
  fi
}
##
pwgid() {
  set -- "$(pw "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}" | cut -d ':' -f 4
  else
    return "${2}"
  fi
}
##
pwgecos() {
  set -- "$(pw "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}" | cut -d ':' -f 5
  else
    return "${2}"
  fi
}
##
pwhome() {
  set -- "$(pw "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}" | cut -d ':' -f 6
  else
    return "${2}"
  fi
}
##
pwshell() {
  set -- "$(pw "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    echo "${1}" | cut -d ':' -f 7
  else
    return "${2}"
  fi
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
uid() {
  id -u "${@}"
}
##
user() {
  uid -n "${@}"
}
##
euid() {
  uid "${@}"
}
##
euser() {
  user "${@}"
}
##
ruid() {
  uid -r "${@}"
}
##
ruser() {
  user -r "${@}"
}
##
gid() {
  id -g "${@}"
}
##
group() {
  gid -n "${@}"
}
##
egid() {
  gid "${@}"
}
##
egroup() {
  group "${@}"
}
##
rgid() {
  gid -r "${@}"
}
##
rgroup() {
  group -r "${@}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
home() {
  set -- "$(euid || :)" "${HOME-}"
  [ -n "${1}" ] || return "${?}"
  if [ -n "${2}" ]                                      &&
           [ -d "${2}"                                ] &&
     noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ]; then
    echo "${2}"
    return
  fi
  set -- "${1}" "$(pwhome "${1}" || :)"
  [ -n "${2}" ] || {
    noerr [ "$(euid || :)" -eq 0 ] && set -- "${1}" '/root'
  } || return "${?}"
  assert       [ -d "${2}"                                ] || return "${?}"
  assert noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ] || return "${?}"
  echo "${2}"
}
##
xdg_cache_home() {
  set -- "$(euid || :)" "${XDG_CACHE_HOME-}"
  [ -n "${1}" ] || return "${?}"
  if [ -n "${2}" ]                                      &&
           [ -d "${2}"                                ] &&
     noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ]; then
    echo "${2}"
    return
  fi
  set -- "${1}" "$(home || :)"
  [ -n "${2}" ] && set -- "${1}" "${2}/.cache"
  [ -d "${2}" ] || {
    noerr [ "$(euid || :)" -eq 0 ] && set -- "${1}" '/var/cache'
  } || return "${?}"
  assert       [ -d "${2}"                                ] || return "${?}"
  assert noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ] || return "${?}"
  echo "${2}"
}
##
xdg_config_home() {
  set -- "$(euid || :)" "${XDG_CONFIG_HOME-}"
  [ -n "${1}" ] || return "${?}"
  if [ -n "${2}" ]                                      &&
           [ -d "${2}"                                ] &&
     noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ]; then
    echo "${2}"
    return
  fi
  set -- "${1}" "$(home || :)"
  [ -n "${2}" ] && set -- "${1}" "${2}/.config"
  [ -d "${2}" ] || {
    noerr [ "$(euid || :)" -eq 0 ] && set -- "${1}" '/etc'
  } || return "${?}"
  assert       [ -d "${2}"                                ] || return "${?}"
  assert noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ] || return "${?}"
  echo "${2}"
}
##
xdg_data_home() {
  set -- "$(euid || :)" "${XDG_DATA_HOME-}"
  [ -n "${1}" ] || return "${?}"
  if [ -n "${2}" ]                                      &&
           [ -d "${2}"                                ] &&
     noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ]; then
    echo "${2}"
    return
  fi
  set -- "${1}" "$(home || :)"
  [ -n "${2}" ] && set -- "${1}" "${2}/.local/share"
  [ -d "${2}" ] || {
    noerr [ "$(euid || :)" -eq 0 ] && set -- "${1}" '/var/lib'
  } || return "${?}"
  assert       [ -d "${2}"                                ] || return "${?}"
  assert noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ] || return "${?}"
  echo "${2}"
}
##
xdg_state_home() {
  set -- "$(euid || :)" "${XDG_STATE_HOME-}"
  [ -n "${1}" ] || return "${?}"
  if [ -n "${2}" ]                                      &&
           [ -d "${2}"                                ] &&
     noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ]; then
    echo "${2}"
    return
  fi
  set -- "${1}" "$(home || :)"
  [ -n "${2}" ] && set -- "${1}" "${2}/.local/state"
  [ -d "${2}" ] || {
    noerr [ "$(euid || :)" -eq 0 ] && set -- "${1}" '/var/lib'
  } || return "${?}"
  assert       [ -d "${2}"                                ] || return "${?}"
  assert noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ] || return "${?}"
  echo "${2}"
}
##
xdg_runtime_dir() {
  set -- "$(euid || :)" "${XDG_RUNTIME_DIR-}"
  [ -n "${1}" ] || return "${?}"
  if [ -n "${2}" ]                                      &&
           [ -d "${2}"                                ] &&
     noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ]; then
    echo "${2}"
    return
  fi
  set -- "${1}" "/run/user/${1}"
  [ -d "${2}" ] || {
    noerr [ "$(euid || :)" -eq 0 ] && set -- "${1}" '/run'
  } || return "${?}"
  assert       [ -d "${2}"                                ] || return "${?}"
  assert noerr [ "$(stat -c '%u' "${2}" || :)" -eq "${1}" ] || return "${?}"
  echo "${2}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
set_USER() {
  set -- "$(euser || :)"
  [ -n "${1}" ] && USER="${1}"
}
##
set_LOGNAME() {
  set -- "$(euser || :)"
  [ -n "${1}" ] && LOGNAME="${1}"
}
##
set_HOME() {
  set -- "$(home || :)"
  [ -n "${1}" ] && HOME="${1}"
}
##
set_XDG_CACHE_HOME() {
  set -- "$(xdg_cache_home || :)"
  [ -n "${1}" ] && XDG_CACHE_HOME="${1}"
}
##
set_XDG_CONFIG_HOME() {
  set -- "$(xdg_config_home || :)"
  [ -n "${1}" ] && XDG_CONFIG_HOME="${1}"
}
##
set_XDG_DATA_HOME() {
  set -- "$(xdg_data_home || :)"
  [ -n "${1}" ] && XDG_DATA_HOME="${1}"
}
##
set_XDG_STATE_HOME() {
  set -- "$(xdg_state_home || :)"
  [ -n "${1}" ] && XDG_STATE_HOME="${1}"
}
##
set_XDG_RUNTIME_DIR() {
  set -- "$(xdg_runtime_dir || :)"
  [ -n "${1}" ] && XDG_RUNTIME_DIR="${1}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
export_USER() {
  set_USER && export USER
}
##
export_LOGNAME() {
  set_LOGNAME && export LOGNAME
}
##
export_HOME() {
  set_HOME && export HOME
}
##
export_XDG_CACHE_HOME() {
  set_XDG_CACHE_HOME && export XDG_CACHE_HOME
}
##
export_XDG_CONFIG_HOME() {
  set_XDG_CONFIG_HOME && export XDG_CONFIG_HOME
}
##
export_XDG_DATA_HOME() {
  set_XDG_DATA_HOME && export XDG_DATA_HOME
}
##
export_XDG_STATE_HOME() {
  set_XDG_STATE_HOME && export XDG_STATE_HOME
}
##
export_XDG_RUNTIME_DIR() {
  set_XDG_RUNTIME_DIR && export XDG_RUNTIME_DIR
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
displaynumber() {
  [ "${#}" -gt 0 ] || set -- "${DISPLAY-:0}"
  set -- "${1##*:}"
  set -- "${1%%.*}"
  noerr [ "${1}" -ge 0 ] && echo "${1}"
}
##
displaylockfile() {
  set -- "$(displaynumber "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    assert noerr [ "${1}" -ge 0 ] || return "${?}"
    echo "/tmp/.X${1}-lock"
  else
    return "${2}"
  fi
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
dbus_session_bus_address_socket() {
  [ "${#}" -gt 0 ] || set -- "${DBUS_SESSION_BUS_ADDRESS-}"
  if [ -n "${1}" ]; then
    set -- "${1}" "${1#*:path=}"
    if [ "${1}" = "${2}" ] || [ -n "${2}" ]; then
      echo "${2}"
    else
      return "${?}"
    fi
  else
    return "${?}"
  fi
}
##
dbus_session_bus_address_uid() {
  set -- "$(dbus_session_bus_address_socket "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    assert [ -n "${1}" ] || return "${?}"
    if isprefix 'unix:abstract=' "${1}"; then
      puid "${DBUS_SESSION_BUS_PID:-0}"
    else
      stat -c '%u' "${1}"
    fi
  else
    return "${2}"
  fi
}
##
dbus_session_bus_address_uid_is_effective() {
  noerr [ "$(dbus_session_bus_address_uid "${@}" || :)" -eq "$(euid || :)" ]
}
##
dbus_session_bus_address_user() {
  set -- "$(dbus_session_bus_address_socket "${@}")" "${?}"
  if [ "${2}" -eq 0 ]; then
    assert [ -n "${1}" ] || return "${?}"
    if isprefix 'unix:abstract=' "${1}"; then
      puser "${DBUS_SESSION_BUS_PID:-0}"
    else
      stat -c '%U' "${1}"
    fi
  else
    return "${2}"
  fi
}
##
dbus_session_bus_address_user_is_effective() {
  [ "$(dbus_session_bus_address_user "${@}" || :)" = "$(euser || :)" ]
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
dbus_ensure() {
  dbus_session_bus_address_uid_is_effective || dbus_launch
}
##
dbus_launch() {
  eval -- "$(export_USER            || unset -v USER
             export_LOGNAME         || unset -v LOGNAME
             export_HOME            || unset -v HOME
             export_XDG_CACHE_HOME  || unset -v XDG_CACHE_HOME
             export_XDG_CONFIG_HOME || unset -v XDG_CONFIG_HOME
             export_XDG_DATA_HOME   || unset -v XDG_DATA_HOME
             export_XDG_STATE_HOME  || unset -v XDG_STATE_HOME
             export_XDG_RUNTIME_DIR || unset -v XDG_RUNTIME_DIR
             exec -- dbus-launch --sh-syntax)" &&
    assert dbus_session_bus_address_uid_is_effective
}
##
dbus_launch_maybe() {
  [ -n "${DBUS_SESSION_BUS_ADDRESS-}" ] || dbus_launch
}
##
dbus_terminate() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ "${#}" -lt 1 ]; then
    set -- "${DBUS_SESSION_BUS_PID:-0}"
    unset -v DBUS_SESSION_BUS_PID
  fi
  if noerr [ "${1}" -gt 0 ]; then
    kill --  "${1}"
  else
    return "${?}"
  fi
}
##
dbus_trap() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ "${#}" -gt 0 ]; then
    __action__="${1}"
    shift
  else
    __action__=
  fi
  traps -- ${__action__:+"${__action__}
"}'dbus_terminate || :' "${@}" || {
    set -- "${?}"
    unset -v __action__
    return "${1}"
  }
  unset -v __action__
}
##
dbus_trap_ensure() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  dbus_session_bus_address_uid_is_effective || dbus_trap_launch "${@}"
}
##
dbus_trap_launch() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  set -- "${DBUS_SESSION_BUS_PID:-0}" "${@}"
  {
    noerr [ "${1}" -eq 0 ] || ! noerr kill -0 -- "${1}"
  } && shift && unset -v DBUS_SESSION_BUS_PID &&
    dbus_trap "${@}" && dbus_launch
}
##
dbus_trap_launch_maybe() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  [ -n "${DBUS_SESSION_BUS_ADDRESS-}" ] || dbus_trap_launch "${@}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
# CAUTION:
#
# GNOME keyring daemon (GKD) may output either
#
#   insufficient process capabilities, insecure memory might get used
#
# or alternatively
#
#   no process capabilities, insecure memory might get used
#
# warning messages as per [3].  This happens when a GKD process is either
# deprived from the 'CAP_IPC_LOCK' capability or alternatively from privilege
# escalation in order to obtain it respectively.  The motivation behind having
# this capability is to be able to lock some memory pages with (unlocked)
# sensitive data (e.g. encryption keys, passwords, certificates, and etc.) in
# random access memory (RAM) using <mlock(2)> or <mlockall(2)>, essentially
# preventing them from being paged out to persistent storage drive
# (i.e. swapping), as otherwise, this would potentially expose (unlocked)
# sensitive data to cold boot attacks [4, 5] targeting that swap memory region
# on persistent storage drive.
#
# Temporary elevation of privileges for regular users is typically achieved
# via 'setuid', which GKD relies upon as well.  However, a good example of
# deprivation from both the 'CAP_IPC_LOCK' capability and privilege escalation
# (e.g. via 'setuid') would be security hardening of <systemd.service(5)>
# using <systemd.exec(5)> option 'NoNewPrivileges' by either explicitly
# setting
#
#   NoNewPrivileges=yes
#
# or implicitly due to setting at least one out of many other security
# hardening options (see documentation for the complete list).  Sadly,
# explicitly including the 'CAP_IPC_LOCK' capability into ambient and bounding
# capability sets by setting 'AmbientCapabilities' and 'CapabilityBoundingSet'
# options is only available for system services and not supported for services
# running in per-user instances of the service manager.
#
# As a result, in case of per-user instances of the service manager, one might
# be concerned with a dilemma to choose one or another:
#
# 1.  Prevent potential swapping of memory pages with unlocked sensitive data.
# 2.  All modern security hardening features for a complete process tree.
#
# Turns out there are multiple compelling reasons to choose 2 over 1:
#
# 1.  If a GKD process at play is supposed to be short-lived (e.g. unlocked to
#     quickly retrieve the required sensitive data and then immediately
#     terminated afterwards), then the probability of swapping of memory pages
#     with (unlocked) sensitive data rapidly approaches zero.
# 2.  There could be bugs in implementation of this feature in GKD.  In
#     general, why should we trust that <mlock(2)> is utilized correctly and
#     truly delivers improved security for all of the (unlocked) sensitive
#     data in RAM?
# 3.  In fact, the second reason facilitates itself as part of the following
#     general assertion: both <mlock(2)> and <mlockall(2)> have no control
#     over suspension and/or hibernation.  That is both cold boot attacks (on
#     suspended RAM and/or hibernated swap storage drive) are still possible
#     unless keyring is locked beforehand as per [4].  This heavily depends on
#     a particular system configuration [4] though and can hardly be ensured
#     to be bulletproof.
#
# Although running a GKD process under 'systemd-inhibit' (strongly
# recommended) should help to detect and defend against cases from the third
# reason to certain extent, it is certainly not a bulletproof solution either.
# Instead, the real practical solution to all these leaks and security
# implications is encryption of swap storage drive.  The most common and
# security-efficient approach is, at the very least, to encrypt a dedicated
# swap storage partition with a randomly generated key at boot time.
#
# Note that cold boot attacks targeting RAM directly are still possible
# regardless, though, they require a more much sophisticated execution to be
# successful, which is still feasible if someone extremely professional
# security-wise (e.g. government) is really after you.
#
gnome_keyring_daemon() (
  pgid=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -p|-pgid|--pgid)
        pgid=1
        ;;
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  for arg in "${@}"; do
    case "${arg}" in
      -f|--foreground)
        assert [ -z "${pgid}" ] || return "${?}"
        break
        ;;
    esac
  done
  dbus_session_bus_address="${DBUS_SESSION_BUS_ADDRESS-}"
  gnome_keyring_control="${GNOME_KEYRING_CONTROL-}"
  unset -v DBUS_SESSION_BUS_ADDRESS
  unset -v GNOME_KEYRING_CONTROL
  unset -v GNOME_KEYRING_DATA_HOME
  unset -v GNOME_KEYRING_LOGIN_KEYRING
  unset -v SSH_AUTH_SOCK
  eval -- "$(export_USER            || unset -v USER
             export_LOGNAME         || unset -v LOGNAME
             export_HOME            || unset -v HOME
             export_XDG_CACHE_HOME  || unset -v XDG_CACHE_HOME
             export_XDG_CONFIG_HOME || unset -v XDG_CONFIG_HOME
             export_XDG_DATA_HOME   || unset -v XDG_DATA_HOME
             export_XDG_STATE_HOME  || unset -v XDG_STATE_HOME
             export_XDG_RUNTIME_DIR || unset -v XDG_RUNTIME_DIR
             if [ -n "${dbus_session_bus_address}" ]; then
                      DBUS_SESSION_BUS_ADDRESS="${dbus_session_bus_address}"
               export DBUS_SESSION_BUS_ADDRESS
             fi
             if [ -n "${gnome_keyring_control}" ]; then
                      GNOME_KEYRING_CONTROL="${gnome_keyring_control}"
               export GNOME_KEYRING_CONTROL
             fi
             if [ -n "${pgid}" ]; then
               getpid pid || return "${?}"
               cat <<- __END__
GNOME_KEYRING_DAEMON_PGID='${pid}';
__END__
               set -- exec_or_fork -- setsid gnome-keyring-daemon "${@}"
             else
               set -- exec         --        gnome-keyring-daemon "${@}"
             fi
             if [ -n "${XDG_DATA_HOME-}" ]; then
               GNOME_KEYRING_DATA_HOME="${XDG_DATA_HOME}/keyrings";
               cat <<- __END__
GNOME_KEYRING_DATA_HOME='${GNOME_KEYRING_DATA_HOME}';
GNOME_KEYRING_LOGIN_KEYRING='${GNOME_KEYRING_DATA_HOME}/login.keyring';
__END__
             fi
             "${@}")"                          || return "${?}"
  [   -z "${GNOME_KEYRING_DAEMON_PGID-}"    ]  ||
    kill -0 -- "-${GNOME_KEYRING_DAEMON_PGID}" || return "${?}"
  [   -z "${GNOME_KEYRING_LOGIN_KEYRING-}"  ]  ||
    [ -f "${GNOME_KEYRING_LOGIN_KEYRING}"   ]  || return "${?}"
  [   -z "${GNOME_KEYRING_CONTROL-}"        ]  ||
    [ -d "${GNOME_KEYRING_CONTROL}"         ]  || return "${?}"
  [   -z "${GNOME_KEYRING_CONTROL-}"        ]  ||
    [ -S "${GNOME_KEYRING_CONTROL}/control" ]  || return "${?}"
  [   -z "${SSH_AUTH_SOCK-}"                ]  ||
    [ -S "${SSH_AUTH_SOCK}"                 ]  || return "${?}"
  if [ -n "${GNOME_KEYRING_DAEMON_PGID-}" ]; then
    cat <<- __END__
GNOME_KEYRING_DAEMON_PGID='${GNOME_KEYRING_DAEMON_PGID}';
__END__
  fi
  if [ -n "${GNOME_KEYRING_LOGIN_KEYRING-}" ]; then
    cat <<- __END__
GNOME_KEYRING_DATA_HOME='${GNOME_KEYRING_DATA_HOME}';
GNOME_KEYRING_LOGIN_KEYRING='${GNOME_KEYRING_LOGIN_KEYRING}';
__END__
  fi
  if [ -n "${GNOME_KEYRING_CONTROL-}" ]; then
    cat <<- __END__
GNOME_KEYRING_CONTROL='${GNOME_KEYRING_CONTROL}';
export GNOME_KEYRING_CONTROL;
__END__
  fi
  if [ -n "${SSH_AUTH_SOCK-}" ]; then
    cat <<- __END__
SSH_AUTH_SOCK='${SSH_AUTH_SOCK}';
export SSH_AUTH_SOCK;
__END__
  fi
  if [ -n "${DBUS_SESSION_BUS_ADDRESS-}" ]; then
    cat <<- __END__
DBUS_SESSION_BUS_ADDRESS='${DBUS_SESSION_BUS_ADDRESS}';
export DBUS_SESSION_BUS_ADDRESS;
__END__
  fi
)
##
gnome_keyring_daemon_ensure() {
  dbus_ensure && gnome_keyring_daemon_unlock "${@}"
}
##
gnome_keyring_daemon_start() {
  __pgid__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -p|-pgid|--pgid)
        __pgid__=1
        ;;
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  eval -- "$(gnome_keyring_daemon ${__pgid__:+-p --} --start "${@}")" &&
    set -- "${?}" || set -- "${?}"
  unset -v __pgid__
  return "${1}"
}
##
gnome_keyring_daemon_start_maybe() {
  [ -n "${GNOME_KEYRING_CONTROL-}" ] || gnome_keyring_daemon_start "${@}"
}
##
gnome_keyring_daemon_unlock() {
   __components__=
  __control_dir__=
         __pgid__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -c|--components)
        assert [ "${#}" -gt 1 ] || return "${?}"
        __components__="${2}"
        shift
        ;;
      -c*)
        __components__="${1#-c}"
        ;;
      --components=*)
        __components__="${1#--components=}"
        ;;
      -C|--control-directory)
        assert [ "${#}" -gt 1 ] || return "${?}"
        __control_dir__="${2}"
        shift
        ;;
      -C*)
        __control_dir__="${1#-C}"
        ;;
      --control-directory=*)
        __control_dir__="${1#--control-directory=}"
        ;;
      -p|-pgid|--pgid)
        __pgid__=1
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ "${#}" -gt 0 ] || set -- :
  eval -- "$("${@}" |
    gnome_keyring_daemon        ${__pgid__:+-p --} --unlock                  \
                          ${__components__:+-c "${__components__}"}          \
                         ${__control_dir__:+-C "${__control_dir__}"})" &&
    set -- "${?}" || set -- "${?}"
  unset -v  __components__
  unset -v __control_dir__
  unset -v        __pgid__
  return "${1}"
}
##
gnome_keyring_daemon_unlock_maybe() {
  [ -n "${GNOME_KEYRING_CONTROL-}" ] || gnome_keyring_daemon_unlock "${@}"
}
##
gnome_keyring_daemon_terminate() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ "${#}" -lt 1 ]; then
    set -- "${GNOME_KEYRING_DAEMON_PGID:-0}"
    unset -v GNOME_KEYRING_DAEMON_PGID
  fi
  killpg "${1}"
}
##
gnome_keyring_daemon_trap() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ "${#}" -gt 0 ]; then
    __action__="${1}"
    shift
  else
    __action__=
  fi
  traps -- ${__action__:+"${__action__}
"}'gnome_keyring_daemon_terminate || :' "${@}" || {
    set -- "${?}"
    unset -v __action__
    return "${1}"
  }
  unset -v __action__
}
##
gnome_keyring_daemon_trap_ensure() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  if dbus_session_bus_address_uid_is_effective; then
    gnome_keyring_daemon_trap
  else
    dbus_trap_launch -- 'gnome_keyring_daemon_terminate || :'
  fi && gnome_keyring_daemon_unlock "${@}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
secret_unlock() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  __control_dir__=
          __ssh__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -s|-ssh|--ssh)
        __ssh__=1
        ;;
      -C|--control-directory)
        assert [ "${#}" -gt 1 ] || return "${?}"
        __control_dir__="${2}"
        shift
        ;;
      -C*)
        __control_dir__="${1#-C}"
        ;;
      --control-directory=*)
        __control_dir__="${1#--control-directory=}"
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -f "${1-}" ] || set --
  gnome_keyring_daemon_trap_ensure -c "secrets${__ssh__:+,ssh}"              \
                ${__control_dir__:+-C "${__control_dir__}"}                  \
                                   -p ${1:+-- cat -- "${1}"} &&
    set -- "${?}" || set -- "${?}"
  unset -v __control_dir__
  unset -v         __ssh__
  return "${1}"
}
##
secret_tool() (
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  password_file="${SECRET_PASSWORD_FILE-}"
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -p|--pw-file|--password-file)
        assert [ "${#}" -gt 1 ] || return "${?}"
        password_file="${2}"
        shift
        ;;
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  secret_unlock ${password_file:+-- "${password_file}"} && secret-tool "${@}"
)
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
noin() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  if [ "${#}" -gt 0 ]; then
    "${@}" 0< '/dev/null'
  else
    [ -t 0 ] && exec 0< '/dev/null'
  fi
}
##
noout() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  if [ "${#}" -gt 0 ]; then
    "${@}" 1> '/dev/null'
  else
    [ -t 1 ] && exec 1> '/dev/null'
  fi
}
##
noerr() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  if [ "${#}" -gt 0 ]; then
    "${@}" 2> '/dev/null'
  else
    [ -t 2 ] && exec 2> '/dev/null'
  fi
}
##
toerr() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  "${@}" 1>&2
}
##
quiet() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  noout "${@}"
}
##
quiet_if_fail() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  set -- "$("${@}")" "${?}"
  if [ "${2}" -eq 0 ] && [ -n "${1}" ]; then
    echo "${1}"
  else
    return "${2}"
  fi
}
##
silent() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  noout noerr "${@}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
error() (
  format="${1}"
  shift
  toerr printf -- "%s${format}\n" 'error: ' "${@}"
)
##
warning() (
  format="${1}"
  shift
  printf -- "%s${format}\n" 'warning: ' "${@}"
)
##
info() (
  format="${1}"
  shift
  printf -- "${format}\n" "${@}"
)
##
debug() (
  format="${1}"
  shift
  printf -- "%s${format}\n" 'debug: ' "${@}"
)
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
assert() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  unset -v SHELL_COMMANDS_TARGET
  fork -- "${@}" || (
    status="${?}"
    error '%s%s%s' 'Failed assertion: '                                      \
          "Failure exit status (${status}):"                                 \
          "$(printf -- " '%s'" "${@}")"
    return "${status}"
  )
}
##
reject() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  unset -v SHELL_COMMANDS_TARGET
  fork -- "${@}" && (
    status="${?}"
    error '%s%s%s' 'Failed rejection: '                                      \
          "Success exit status (${status}):"                                 \
          "$(printf -- " '%s'" "${@}")"
    return "${status}"
  )
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
isprefix() {
  [ "${1}${2#${1}}" = "${2}" ]
}
##
issuffix() {
  [ "${2%${1}}${1}" = "${2}" ]
}
##
join() {
  printf -- "${join_prefix-${prefix-}}%s${join_suffix-${suffix-}}" "${1}"
  shift
  if [ "${#}" -gt 0 ]; then
    printf -- "${join_delimiter-${delimiter-}}"\
"${join_prefix-${prefix-}}%s${join_suffix-${suffix-}}" "${@}"
  fi
}
##
enclose() (
  delimiter="${enclose_delimiter-${delimiter- }}"
  prefix="${1}"
  suffix="${2}"
  shift
  shift
  join "${@}"
)
##
prefix() (
  delimiter="${prefix_delimiter-${delimiter- }}"
  prefix="${1}"
  shift
  enclose "${prefix}" '' "${@}"
)
##
suffix() (
  delimiter="${suffix_delimiter-${delimiter- }}"
  suffix="${1}"
  shift
  enclose '' "${suffix}" "${@}"
)
##
exclude() {
  include -n "${@}"
}
##
include() {
  __bre__=
  __ere__=
   __fs__=1
  __not__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -b|-bre|--bre)
        assert [ -z "${__ere__}" ] || return "${?}"
        __bre__=1
         __fs__=
        ;;
      -e|-ere|--ere)
        assert [ -z "${__bre__}" ] || return "${?}"
        __ere__=1
         __fs__=
        ;;
      -n|--not)
        if [ -n "${__not__}" ]; then
          __not__=
        else
          __not__=1
        fi
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ "${#}" -gt 0 ]; then
    grep ${__ere__:+-E} ${__fs__:+-F -x} ${__not__:+-v} $(prefix '-e ' "${@}")
  else
    cat
  fi && set -- "${?}" || set -- "${?}"
  unset -v __bre__
  unset -v __ere__
  unset -v  __fs__
  unset -v __not__
  return "${1}"
}
##
range() {
  case "${#}" in
    1)
      echo "for (i = 1; i <= ${1}; i += 1) i" | bc -l
      ;;
    2)
      echo "for (i = ${1}; i <= ${2}; i += 1) i" | bc -l
      ;;
    3)
      echo "for (i = ${1}; i <= ${3}; i += ${2}) i" | bc -l
      ;;
  esac
}
##
trim() {
  set -- "${1#${1%%[![:blank:]]*}}"
  set -- "${1%${1##*[![:blank:]]}}"
  printf -- '%s' "${1}"
}
##
escapeuri() {
  perl -M'URI::Escape' -e 'print uri_escape($ARGV[0], $ARGV[1]);'            \
       "${1}" "${2-^A-Za-z0-9\-\._~/}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
fd() {
  if [ "${#}" -lt 1 ]; then
    getpid __pid__ || return "${?}"
    set -- "${__pid__}"
    unset -v __pid__
  fi
  if noerr [ "${1}" -gt 0 ]; then
    if quiet command_name -- lsof; then
      set -- "$(lsof -F f -p "${1}" -w)" "${?}"
      if [ "${2}" -eq 0 ]; then
        echo "${1}" | grep -e 'f[0-9]' | cut -c 2-
      else
        return "${2}"
      fi
    else
      if kill -0 -- "${1}"; then
        range 0 255
      else
        return "${?}"
      fi
    fi
  else
    return "${?}"
  fi
}
##
close() {
  if [ "${#}" -lt 1 ]; then
    getpid __pid__ || return "${?}"
    set -- "${__pid__}"
    unset -v __pid__
    if noerr [ "${1}" -gt 0 ]; then
      set -- "$(fd "${1}")" "${?}"
    else
      return "${?}"
    fi
    if [ "${2}" -eq 0 ]; then
      set -- $(echo "${1}" | exclude 0 1 2)
    else
      return "${2}"
    fi
  fi
  for __fd__ in "${@}"; do
    eval -- "exec ${__fd__}>&-"
  done
  unset -v __fd__
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
rmdirr() {
  rmfile "${@}" -r
}
##
rmdirr_trap() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -n "${1-}" ] && traps -- 'rmdirr -- "${'"${1}"'}" || :'
}
##
rmdirr_trap_mk() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -n "${1-}" ] && rmdirr_trap -- "${1}" &&
    eval -- "${1}='$(shift && exec_or_fork "${@}")'"
}
##
rmfile() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ -n "${1-}" ]; then
    set -- "${@}" -- "${1}"
    shift
    rm -f "${@}"
  fi
}
##
rmfile_trap() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -n "${1-}" ] && traps -- 'rmfile -- "${'"${1}"'}" || :'
}
##
rmfile_trap_mk() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -n "${1-}" ] && rmfile_trap -- "${1}" &&
    eval -- "${1}='$(shift && exec_or_fork "${@}")'"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
mkslock() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  assert [ "${#}" -gt 0 ] || return "${?}"
  assert [ "${#}" -le 2 ] || return "${?}"
  if [ -d "${2-}" ]; then
    set -- "${1}" "${2%/}/${1##*/}"
  else
    set -- "${1}" "${2:-${1##*/}}"
  fi
  quiet ln -s -- "${@}" && __abspath__ "${2}"
}
##
rmslock() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ -n "${1-}" ]; then
    if [ -L "${1}" ]; then
      assert ! issuffix '/' "${1}" || return "${?}"
      set -- "$(
        getpid pid || :
        set -- "${1}" "${1%/*}/.#${1##*/}.${pid:-${$}}~"
        printf -- '%s' "${2}"
        quiet exec -- mv -f -- "${@}"
      )" "${?}"
      if [ "${2}" -eq 0 ]; then
        rm -f -- "${1}"
      else
        return "${2}"
      fi
    else
      [ ! -e "${1}" ]
    fi
  fi
}
##
rmslock_trap() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -n "${1-}" ] && traps -- 'rmslock -- "${'"${1}"'}" || :'
}
##
rmslock_trap_mk() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  [ -n "${1-}" ] && rmslock_trap -- "${1}" &&
    eval -- "${1}='$(shift && exec_or_fork "${@}")'"
}
##
trap_mkslock() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  __var__="${1-}"
  shift
  rmslock_trap_mk -- "${__var__}" -- mkslock "${@}" &&
    set -- "${?}" || set -- "${?}"
  unset -v __var__
  return "${1}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
mkstemp() {
  __func__='s'
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -d|--dir|--directory)
        # --------------------------------------------------------------------
        # TODO:
        #
        # Unfortunately, there is no built-in macro 'mkdtemp' defined by 'm4':
        #
        : __func__='d'
        # --------------------------------------------------------------------
        shift
        set -- "$(mkstemp "${@}")" "${?}"
        if [ "${2}" -eq 0 ]; then
          [ -d "${1}" ] || {
            rmfile -- "${1}" && mkdir -m 0700 -- "${1}" && echo "${1}"
          }
          return
        else
          return "${2}"
        fi
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  __func__="mk${__func__}temp"
  # --------------------------------------------------------------------------
  # NOTE:
  #
  # As per <mktemp(1)>:
  #
  set -- "${1-tmp.}"
  # --------------------------------------------------------------------------
  # NOTE:
  #
  # As per <mkdtemp(3p)>:
  #
  # > Portable applications should pass exactly six trailing 'X's in the
  # > template and no more; implementations may treat any additional trailing
  # > 'X's as either a fixed or replaceable part of the template. To be sure
  # > of only passing six, a fixed string of at least one non-'X' character
  # > should precede the six 'X's.
  #
  set -- "${1%${1##*[!X]}}XXXXXX"
  # --------------------------------------------------------------------------
  if ! isprefix '/'   "${1}"; then
    set --          ${TMPDIR:+"${TMPDIR}/${1}"}                              \
                      ${TEMP:+"${TEMP}/${1}"}                                \
                       ${TMP:+"${TMP}/${1}"}                                 \
           ${XDG_RUNTIME_DIR:+"${XDG_RUNTIME_DIR}/${1}"}                     \
                              "/run/user/$(euid || echo 0)/${1}"             \
                              "/tmp/${1}"                                    \
                              "/var/tmp/${1}"                                \
                              "${1}"
  fi
  for __template__ in "${@}"; do
    if echo "${__func__}(template)" | m4 -D "template=${__template__}"; then
      set -- "${?}"
      break
    else
      set -- "${?}"
    fi
  done
  unset -v     __func__
  unset -v __template__
  return "${1}"
}
##
trap_mkstemp() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  __var__="${1-}"
  shift
  rmfile_trap_mk -- "${__var__}" -- mkstemp "${@}" &&
    set -- "${?}" || set -- "${?}"
  unset -v __var__
  return "${1}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
mkdtemp() {
  mkstemp -d "${@}"
}
##
trap_mkdtemp() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  __var__="${1-}"
  shift
  rmdirr_trap_mk -- "${__var__}" -- mkdtemp "${@}" &&
    set -- "${?}" || set -- "${?}"
  unset -v __var__
  return "${1}"
}
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
fork() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  case "${1}" in
    exec|exit|return)
      ("${@}")
      ;;
    !)
      shift
      ! "${@}"
      ;;
    *)
      "${@}"
      ;;
  esac
}
##
exec_or_fork() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  case "$(command_name -- "${1}" || :)" in
    */*)
      if trapped; then
        fork -- "${@}"
      else
        exec -- "${@}"
      fi
      ;;
    *)
      fork -- "${@}"
      ;;
  esac
}
##
daemon() (
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  unset -v SHELL_COMMANDS_TARGET
  # --------------------------------------------------------------------------
  # NOTE:
  #
  # The following snippet implements the classic and well-known daemon
  # spawning approach via double-forking:
  #
  # 1.  'fork' --- create the first child process that is not the process
  #     group leader (the surrounding subshell);
  # 2.  'setsid' --- create the new session, set the process group ID (become
  #     the process group leader of the newly created process group), and
  #     execute ('execve') the specified program (the non-interactive shell to
  #     create the second child process);
  # 3.  'fork' --- create the second child process to execute the actual
  #     intended program as daemon (the non-interactive shell applying control
  #     operator <ampersand> '&' to the positional arguments representing the
  #     actual intended program to be executed as daemon).
  #
  # If job control is disabled (and it is here), both utility 'setsid' and
  # control operator <ampersand> '&' (asynchronous execution of the preceding
  # command) redirect the standard input, before any explicit redirection(s)
  # are performed, to '/dev/null'.  Hence, there is no need to explicitly
  # apply that redirection, so only file descriptors of the standard output
  # and the standard error require explicit treatment:
  #
  close && CDPATH= cd -- '/' && umask 0 &&
    exec_or_fork -- setsid sh -e -u -c '"${@}" 1> /dev/null 2>&1 &'          \
                    daemon "${@}"
  # --------------------------------------------------------------------------
)
##
lock() {
    __already__=
      __close__=
  __directory__=
  __exclusive__=
    __no_fork__=1
       __path__="${0}"
     __shared__=
    __verbose__=
       __wait__=
    __command__="${0}"
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -a|--already)
        __already__=1
        ;;
      -c|--close)
          __close__=1
        __no_fork__=
        ;;
      -d|--dir|--directory)
        __directory__=1
        ;;
      -e|-x|--exclusive)
        assert [ -z "${__shared__}" ] || return "${?}"
        __exclusive__=1
        ;;
      -f|--fork)
        __no_fork__=
        ;;
      -p|--path)
        assert [ "${#}" -gt 1 ] || return "${?}"
        __path__="${2}"
        shift
        ;;
      -s|--shared)
        assert [ -z "${__exclusive__}" ] || return "${?}"
        __shared__=1
        ;;
      -v|--verbose)
        __verbose__=1
        ;;
      -w|--wait|-t|--timeout)
        assert [ "${#}" -gt 1 ] || return "${?}"
        __wait__="${2#infinity}"
        shift
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ -z "${__shared__}" ]; then
    __exclusive__=1
  fi
  __path__="$(__realpath__ "${__path__}")" || return "${?}"
  if [ "${#}" -gt 0 ]; then
    __command__="${1}"
    shift
  fi
  set --   ${__no_fork__:+--no-fork}                                         \
         ${__exclusive__:+--exclusive}                                       \
             ${__close__:+--close}                                           \
            ${__shared__:+--shared}                                          \
         "${__path__}" "${__command__}" "${@}"
  __id__="$(printf -- '%s\0' "${@}" | sha1sum | awk '{ print $1 }')"
  if [ "${SHELL_COMMANDS_LOCK_PID:-0}" -eq "${$}" ] || {
       ispplive && [ "${SHELL_COMMANDS_LOCK_PID:-0}" -eq "${PPID}" ]
     }; then
    case " ${SHELL_COMMANDS_LOCK_IDS-} " in
      *" ${__id__} "*)
        unset -v   __already__
        unset -v     __close__
        unset -v __directory__
        unset -v __exclusive__
        unset -v   __no_fork__
        unset -v      __path__
        unset -v    __shared__
        unset -v   __verbose__
        unset -v      __wait__
        unset -v   __command__
        unset -v        __id__
        return
        ;;
      *)
        ;;
    esac
  fi
  getpid SHELL_COMMANDS_LOCK_PID
  if [ "${SHELL_COMMANDS_LOCK_PID}" -eq "${$}" ]; then
           SHELL_COMMANDS_LOCK_IDS="${SHELL_COMMANDS_LOCK_IDS-} ${__id__}"
    export SHELL_COMMANDS_LOCK_IDS
    export SHELL_COMMANDS_LOCK_PID
  else
    unset -v SHELL_COMMANDS_LOCK_IDS
    unset -v SHELL_COMMANDS_LOCK_PID
  fi
  if [ -n "${__already__}" ]; then
    if [ -n "${__directory__}" ]; then
      assert [ -d "${__path__}" ]
    else
      assert [ -e "${__path__}" ]
    fi
  elif [ -n "${__directory__}" ]; then
    mkdir -p -- "${__path__}"
  fi || exit "${?}"
  exec -- flock    ${__wait__:+--wait "${__wait__}"}                         \
                ${__verbose__:+--verbose}                                    \
                "${@}"
}
##
retry() (
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  unset -v SHELL_COMMANDS_TARGET
  count=
  interval=
  quiet=
  timeout=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -c|--count)
        assert [ "${#}" -gt 1 ] || return "${?}"
        count="${2}"
        shift
        ;;
      -i|--interval)
        assert [ "${#}" -gt 1 ] || return "${?}"
        interval="${2}"
        shift
        ;;
      -q|--quiet)
        quiet=1
        ;;
      -t|--timeout|-w|--wait)
        assert [ "${#}" -gt 1 ] || return "${?}"
        timeout="${2#infinity}"
        shift
        ;;
      --)
        shift
        break
        ;;
      -?*)
        error '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  case "${1}" in
    exec)
      shift
      if [ "${#}" -gt 0 ]; then
        case "${1}" in
          --)
            shift
            ;;
        esac
      fi
      ;;
    exit|return)
      quiet=
      timeout=
      ;;
  esac
  if [ -n "${timeout}" ]; then
    case "${1}" in
      command)
        shift
        if [ "${#}" -gt 0 ]; then
          case "${1}" in
            --)
              shift
              ;;
          esac
        fi
        noerr unset -f "${1}" || :
        ;;
    esac
    case "$(command_name -- "${1}" || :)" in
      */*)
        ;;
      *)
        error '%s: %s' "${1}" 'No such executable'
        return 1
        ;;
    esac
    set -- timeout --foreground -k "${timeout}" "${timeout}" "${@}"
  fi
  i=1
  status=0
  # --------------------------------------------------------------------------
  # TODO:
  #
  # The following loop does not support standard input (e.g. either piping or
  # standard input redirection) throughout more than one iteration:
  #
  while ! {
      if [ -n "${quiet}" ]; then
        # --------------------------------------------------------------------
        # TODO:
        #
        # The following output capture does not support binary data (e.g. null
        # characters):
        #
        output="$("${@}")"
        # --------------------------------------------------------------------
      else
        fork -- "${@}"
      fi || {
        status="${?}"
        [ "${i}" -ge "${count:-5}" ]
      }
    }; do
    : $((i = i + 1))
    status=0
    sleep "${interval:-0.1}"
  done
  # --------------------------------------------------------------------------
  if [ -n "${quiet}" ] && [ -n "${output}" ]; then
    echo "${output}"
  fi
  return "${status}"
)
##  ==========================================================================
##  }}} Functions
##
### Shortcut Functions {{{
##  ==========================================================================
if [ -n "${SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS-}" ]; then
  n0() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    noin "${@}"
  }
  ##
  n1() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    noout "${@}"
  }
  ##
  n2() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    noerr "${@}"
  }
  ##
  t2() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    toerr "${@}"
  }
  ##
  q() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    quiet "${@}"
  }
  ##
  qif() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    quiet_if_fail "${@}"
  }
  ##
  s() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    silent "${@}"
  }
  # --------------------------------------------------------------------------
  ##
  # --------------------------------------------------------------------------
  e() {
    error "${@}"
  }
  ##
  w() {
    warning "${@}"
  }
  ##
  i() {
    info "${@}"
  }
  ##
  d() {
    debug "${@}"
  }
  ##
  a() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    assert "${@}"
  }
  ##
  r() {
    if [ -n "${ZSH_VERSION-}" ]; then
      emulate -L sh
    fi
    reject "${@}"
  }
fi
##  ==========================================================================
##  }}} Shortcut Functions
##
### Internal Variables {{{
##  ==========================================================================
if [ -z "${SHELL_COMMANDS_IMPORT_INTERNAL_VARIABLES-}" ]; then
  unset -v         delimiter
  unset -v    join_delimiter
  unset -v enclose_delimiter
  unset -v  prefix_delimiter
  unset -v  suffix_delimiter
  unset -v         prefix
  unset -v    join_prefix
  unset -v         suffix
  unset -v    join_suffix
fi
##  ==========================================================================
##  }}} Internal Variables
##
### References {{{
##  ==========================================================================
##  [1] http://pubs.opengroup.org/onlinepubs/009695299/utilities/trap.html
##  [2] http://www.freedesktop.org/software/systemd/man/systemd.exec.html#IgnoreSIGPIPE=
##  [3] http://github.com/GNOME/gnome-keyring/blob/master/daemon/gkd-capability.c
##  [4] http://wiki.gnome.org/Projects/GnomeKeyring/SecurityFAQ
##  [5] http://en.wikipedia.org/wiki/Cold_boot_attack
##  ==========================================================================
##  }}} References

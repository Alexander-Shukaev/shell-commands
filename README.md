Shell Commands
==============

Table of Contents
=================

-   [About](#markdown-header-about)
-   [Installation](#markdown-header-installation)
    -   [Requirements](#markdown-header-requirements)
        -   [Platforms](#markdown-header-platforms)

About
=====

A set of robust and highly portable [POSIX][POSIX/Wikipedia]-compliant [Unix
shell][Unix shell/Wikipedia] commands.

Installation
============

Requirements
------------

### Platforms

-   Unix;
-   MSYS2;
-   Cygwin.

[POSIX/Wikipedia]: http://en.wikipedia.org/wiki/POSIX

[Unix shell/Wikipedia]: http://en.wikipedia.org/wiki/Unix_shell
